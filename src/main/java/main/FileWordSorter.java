package main;

import main.comparators.BaseComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class FileWordSorter {
    private List<String> words= new LinkedList<>();
    private BaseComparator comparator;
    private static final Logger classLogger = LogManager.getLogger(FileWordSorter.class);

    private void readText(String path) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8))) {
            String line = reader.readLine();

            while (line != null) {
                Collections.addAll(words,line.split(" "));
                line = reader.readLine();
            }
        }
    }

    public void writeSorted(String out) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), StandardCharsets.UTF_8))) {
            words.sort(comparator::compare);
            for (String word : words) {
                classLogger.trace(word);
                writer.write(word+" ");
            }
        }
    }

    public FileWordSorter(String path, BaseComparator comparator) throws IOException {
        this.comparator = comparator;

        readText(path);
    }
}
