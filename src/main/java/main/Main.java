package main;

import main.comparators.BaseComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.InvalidParameterException;


public class Main {
    static final Logger LOG = LogManager.getRootLogger();

    public static void main(String[] args) throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
        String inPath;
        String outPath;
        String comparatorClassName;

        if (args.length == 2) {
            inPath = args[0];
            outPath = args[1];
            comparatorClassName = "main.comparators.AlphabetComparator";
        } else if (args.length == 3) {
            inPath = args[0];
            outPath = args[1];
            comparatorClassName = args[2];
        } else {
            LOG.error("Неправильные аргументы");
            throw new InvalidParameterException("Неправильные аргументы");
        }

        LOG.info("using "+comparatorClassName);

        FileWordSorter sorter = new FileWordSorter(inPath, BaseComparator.getInstance(comparatorClassName));
        sorter.writeSorted(outPath);
    }
}
