package main.comparators;


public class WordLengthComparator extends BaseComparator {
    public int compare(String s, String t1) {
        return s.length() - t1.length();
    }
}