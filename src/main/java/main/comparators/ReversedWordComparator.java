package main.comparators;


public class ReversedWordComparator extends BaseComparator {

        @Override
        public int compare(String s, String t1) {
                StringBuilder sb = new StringBuilder(s);
                StringBuilder t1b = new StringBuilder(t1).reverse();

                s = sb.reverse().toString();
                return s.compareTo(t1b.toString());
        }
}
