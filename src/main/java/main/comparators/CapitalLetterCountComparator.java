package main.comparators;


public class CapitalLetterCountComparator extends BaseComparator {
        @Override
        public int compare(String s, String t1) {
                return capitalLetterCount(s) - capitalLetterCount(t1);
        }
        private int capitalLetterCount(String s) {
                int result = 0;
                for (int i = 0; i < s.length(); i++) {
                        if (Character.isUpperCase(s.charAt(i))) {
                                result++;
                        }
                }
                return result;
        }
}
