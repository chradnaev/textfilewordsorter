package main.comparators;


public class UniqueSymbolComparator extends BaseComparator {

    @Override
    public int compare(String s, String t1) {
        StringBuilder b1 = new StringBuilder(s);
        StringBuilder b2 = new StringBuilder(t1);

        deleteDuplicates(b1);
        deleteDuplicates(b2);

        return b1.toString().compareTo(b2.toString());
    }

    private void deleteDuplicates(StringBuilder builder) {
        for (int i = 0; i < builder.length(); i++) {
            for (int j = i+1; j < builder.length(); j++) {
                if (builder.charAt(i) == builder.charAt(j)) {
                    builder.deleteCharAt(j);
                    j--;
                }
            }
        }
    }
}
