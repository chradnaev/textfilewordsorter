package main.comparators;


public abstract class BaseComparator {
    public abstract int compare(String s, String t1);

    public static BaseComparator getInstance(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> comparatorClass = Class.forName(className);

        return (BaseComparator)comparatorClass.newInstance();
    }
}
