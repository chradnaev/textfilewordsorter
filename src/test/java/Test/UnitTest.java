package Test;

import main.FileWordSorter;
import main.comparators.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import java.io.*;


public class UnitTest {
    private static final String testInFilename = "inTest.txt";
    private static final String testOutFilename = "outTest.txt";
    private static final Logger testLogger = LogManager.getLogger(UnitTest.class);

    @Test
    public void testFileWordSorter() throws IOException {
        String testString = "asd qwe cvb aqd";
        String testCorrectString = "aqd asd cvb qwe";
        BaseComparator comparator = new AlphabetComparator();
        testLogger.trace("testing '"+testString+"' and '"+testCorrectString+"' with "+CapitalLetterCountComparator.class);

        writeStringToFile(testString,testInFilename);

        FileWordSorter sorter = new FileWordSorter(testInFilename, comparator);
        sorter.writeSorted(testOutFilename);

        Assert.assertEquals(readStringFromFile(testOutFilename).trim(),testCorrectString);
    }

    @Test
    public void testAlphabetComparator() {
        testLogger.trace("Testing "+AlphabetComparator.class);
        BaseComparator comparer = new AlphabetComparator();
        Assert.assertTrue(comparer.compare("qwe","asz") > 0);
        Assert.assertEquals(comparer.compare("qwe","qwe"), 0);
        Assert.assertTrue(comparer.compare("breeks","zrada") < 0);
    }

    @Test
    public void testCapitalLetterComparator() {
        testLogger.trace("Testing "+CapitalLetterCountComparator.class);
        BaseComparator comparer = new CapitalLetterCountComparator();
        Assert.assertTrue(comparer.compare("qWEe","asZ") > 0);
        Assert.assertEquals(comparer.compare("qWe","Qwe"), 0);
        Assert.assertTrue(comparer.compare("zrada","BREEKS") < 0);
    }

    @Test
    public void testReversedWordComparator() {
        testLogger.trace("Testing "+ReversedWordComparator.class);
        BaseComparator comparer = new ReversedWordComparator();
        Assert.assertTrue(comparer.compare("broz","ty") > 0);
        Assert.assertEquals(comparer.compare("erty","erty"), 0);
        Assert.assertTrue(comparer.compare("zrada","breeks") < 0);
    }

    @Test
    public void testUniqueSymbolComparator() {
        testLogger.trace("Testing "+UniqueSymbolComparator.class);
        BaseComparator comparer = new UniqueSymbolComparator();
        Assert.assertTrue(comparer.compare("qWWWWz","qWWWWWWWWa") > 0);
        Assert.assertEquals(comparer.compare("ASSSSD","ASD"), 0);
        Assert.assertTrue(comparer.compare("aSSSa","bSSSa") < 0);
    }

    @Test
    public void testWordlengthComparator() {
        testLogger.trace("Testing "+WordLengthComparator.class);
        BaseComparator comparer = new WordLengthComparator();
        Assert.assertTrue(comparer.compare("qweeeez","asd") > 0);
        Assert.assertEquals(comparer.compare("qwe","ras"), 0);
        Assert.assertTrue(comparer.compare("sd","breeks") < 0);
    }

    private void writeStringToFile(String st, String path) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write(st);
        }
    }

    private String readStringFromFile(String path) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                builder.append(line);
                line = reader.readLine();
            }
            return builder.toString();
        }
    }

    @After
    public void afterTest() {
        File file = new File(testInFilename);
        if (file.exists()) {
            file.delete();
        }

        File file1 = new File(testOutFilename);
        if (file1.exists()) {
            file1.delete();
        }
    }
}
